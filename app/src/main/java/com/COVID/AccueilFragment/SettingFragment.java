package com.COVID.AccueilFragment;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.COVID.MycovidDiary.R;
import com.COVID.MycovidDiary.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


/**
 * A simple {@link Fragment} subclass.
 */
public class SettingFragment extends Fragment {

    private TextView email;
    private FirebaseAuth auth;
    private Button change;
    private EditText password;
    private ProgressDialog dialog;

    public SettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview=inflater.inflate(R.layout.fragment_setting,container,false);
        email=(TextView) rootview.findViewById(R.id.email);
        change=(Button) rootview.findViewById(R.id.btn_change);
        password=(EditText) rootview.findViewById(R.id._password);
         dialog = new ProgressDialog(getContext());;
        auth = FirebaseAuth.getInstance();


        return rootview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final FirebaseUser user=auth.getCurrentUser();
        assert user != null;
        String mail=user.getEmail();
        email.setText(mail);
        change.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(user!=null){
                    dialog.setMessage("Changing password,Please Wait !");
                    dialog.show();
user.updatePassword(password.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
    @Override
    public void onComplete(@NonNull Task<Void> task) {
        if(task.isSuccessful()){
            dialog.dismiss();
            Toast.makeText(getContext(),"Your Password has been changed",Toast.LENGTH_LONG).show();
            auth.signOut();
            getActivity().finish();
            Intent intent=new Intent(getContext(), User.class);
            startActivity(intent);
        }else {
            dialog.dismiss();
            Toast.makeText(getContext(),"Password could not be changed",Toast.LENGTH_LONG).show();

        }
    }
});
                }
            }
        });


    }
}
