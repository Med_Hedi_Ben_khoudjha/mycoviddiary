package com.COVID.AccueilFragment;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.COVID.MycovidDiary.Entity.person_contact;
import com.COVID.MycovidDiary.R;
import com.COVID.mycovid_diary.MyListAdapter;
import com.daimajia.swipe.util.Attributes;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class HistoryActivity extends AppCompatActivity {
FirebaseDatabase database;
DatabaseReference reference;
ArrayList<person_contact>person_contacts;
person_contact person_contact;
    MyListAdapter adapter;
     RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
         recyclerView = (RecyclerView) findViewById(R.id.recyclerView);


        database=FirebaseDatabase.getInstance();
        reference=database.getReference("person_contact");
person_contacts=new ArrayList<>();
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot ds:snapshot.getChildren()){
                    person_contact =new person_contact();

                    person_contact=ds.getValue(com.COVID.MycovidDiary.Entity.person_contact.class);
person_contacts.add(person_contact);
                }
                superview(person_contacts);


            }




            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    long animationduration =1000;
    private void superview(ArrayList<person_contact> productList) {

        LinearLayoutManager firstManager = new LinearLayoutManager(this);

        //layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(firstManager);

        recyclerView.setHasFixedSize(true);
        MyListAdapter apter=new MyListAdapter(this,productList);




        ((MyListAdapter) apter).setMode(Attributes.Mode.Single);

        recyclerView.setAdapter(apter);

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                Log.e("RecyclerView", "onScrollStateChanged");
            }
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
            }
        });
        //recycler.setAdapter(apter);
        // adapter.notifyDataSetChanged();
    }


}