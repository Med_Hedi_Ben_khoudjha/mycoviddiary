package com.COVID.mycovid_diary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.COVID.MycovidDiary.Home;
import com.COVID.MycovidDiary.MainActivity;
import com.COVID.MycovidDiary.R;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

public class Statistics extends AppCompatActivity {

    ImageView back;
    TextView tvCases, tvRecovered, tvCritical, tvActive, tvTodayCases, tvTotalDeaths, tvTodayDeaths, tvAffectedCountries;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);
        fetchdata();

        back = findViewById(R.id.b);
        tvCases = findViewById(R.id.tvCases);
        tvRecovered = findViewById(R.id.tvRecovered);
        tvCritical = findViewById(R.id.tvCritical);
        tvActive = findViewById(R.id.tvActive);

        tvTotalDeaths = findViewById(R.id.tvTotalDeaths);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent i = new Intent(Statistics.this, Home.class);
                startActivity(i);
                finish();

            }
        });

    }
        private void fetchdata()
        {

            // Create a String request
            // using Volley Library
            String url = "https://corona.lmao.ninja/v2/all";

            StringRequest request
                    = new StringRequest(
                    Request.Method.GET,
                    url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response)
                        {

                            // Handle the JSON object and
                            // handle it inside try and catch
                            try {

                                // Creating object of JSONObject
                                JSONObject jsonObject
                                        = new JSONObject(
                                        response.toString());

                                // Set the data in text view
                                // which are available in JSON format
                                // Note that the parameter inside
                                // the getString() must match
                                // with the name given in JSON format
                                tvCases.setText(
                                        jsonObject.getString(
                                                "cases"));
                                tvRecovered.setText(
                                        jsonObject.getString(
                                                "recovered"));
                                tvCritical.setText(
                                        jsonObject.getString(
                                                "critical"));
                                tvActive.setText(
                                        jsonObject.getString(
                                                "active"));
//                                tvTodayCases.setText(
  //                                      jsonObject.getString(
    //                                            "todayCases"));
                                tvTotalDeaths.setText(
                                        jsonObject.getString(
                                                "deaths"));
                               // tvTodayDeaths.setText(
                                 //       jsonObject.getString(
                                   //             "todayDeaths"));
                                //tvAffectedCountries.setText(
                                  //      jsonObject.getString(
                                    //            "affectedCountries"));
                            }
                            catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error)
                        {
                            Toast.makeText(
                                    Statistics.this,
                                    error.getMessage(),
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });

            RequestQueue requestQueue
                    = Volley.newRequestQueue(this);
            requestQueue.add(request);
        }


    // Task Completed
    // Thanks for watching
    //See you in the next video.
}
