package com.COVID.mycovid_diary;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.COVID.AccueilFragment.HomeFragment;
import com.COVID.MycovidDiary.R;

public class SuiviActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView( R.layout.activity_suivi);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
        {

            //Setting a dynamic title at runtime. Here, it displays the current time.
            actionBar.setTitle("Suivi");
        }

        setfragment(new Person_Contact_Fragment());



    }


    private void setfragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right, R.anim.slideout_from_left);
        fragmentTransaction.replace(R.id.activity_frame_layout, fragment);
        fragmentTransaction.commit();
    }
}