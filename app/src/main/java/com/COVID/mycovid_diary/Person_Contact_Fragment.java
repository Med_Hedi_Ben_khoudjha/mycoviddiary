package com.COVID.mycovid_diary;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.COVID.MycovidDiary.Entity.SessionManagement;
import com.COVID.MycovidDiary.Entity.person_contact;
import com.COVID.MycovidDiary.Home;
import com.COVID.MycovidDiary.R;
import com.COVID.MycovidDiary.RegisterFragment.LoginFragment;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;


public class Person_Contact_Fragment extends Fragment {
EditText npc,input_num,input_namePre;
Button button_confirm;
    private ProgressBar progressBar;
    private FirebaseAuth auth;
    RadioGroup radio_group_Q5;
    private RadioButton radioSexButton;

    public Person_Contact_Fragment() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_person__contact_, container, false);
        auth = FirebaseAuth.getInstance();

        npc=view.findViewById(R.id.NPC);
        radio_group_Q5=view.findViewById(R.id.radio_group_Q5);
        input_num=view.findViewById(R.id.input_num);
        input_namePre=view.findViewById(R.id.input_namePre);
        button_confirm=view.findViewById(R.id.button_confirm);
        progressBar=(ProgressBar) view.findViewById(R.id.progressBar);

// get selected radio button from radioGroup
        int selectedId = radio_group_Q5.getCheckedRadioButtonId();

        // find the radiobutton by returned id
        radioSexButton = (RadioButton) view.findViewById(selectedId);

   return view;
    }
    private person_contact person_contact;
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        button_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String numbre_per_cont = npc.getText().toString().trim();
                final String numero_per_cont = input_num.getText().toString().trim();
                final String name_prenom = input_namePre.getText().toString().trim();
                RadioButton rb = (RadioButton) radio_group_Q5.findViewById(radio_group_Q5.getCheckedRadioButtonId());
                final String circonstances_contact = rb.getText().toString().trim();

                if (TextUtils.isEmpty(numbre_per_cont)) {
                    Toast.makeText(getContext(), "Enter numbre_per_cont address!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(numero_per_cont)) {
                    Toast.makeText(getContext(), "Enter numero_per_cont address!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(name_prenom)) {
                    Toast.makeText(getContext(), "Enter name_prenom address!", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (TextUtils.isEmpty(circonstances_contact)) {
                    Toast.makeText(getContext(), "Enter circonstances_contact address!", Toast.LENGTH_SHORT).show();
                    return;
                }
                progressBar.setVisibility(View.VISIBLE);
                SessionManagement sessionManagement = new SessionManagement(getActivity());
                sessionManagement.getSession();
                String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());

                person_contact=new person_contact(numero_per_cont,
                        numbre_per_cont,
                        name_prenom,
                        circonstances_contact,
                        sessionManagement.getSession(),
                        currentDate);

                FirebaseDatabase.getInstance().getReference("person_contact")
                        .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                        .setValue(person_contact).addOnCompleteListener(new OnCompleteListener<Void>() {

                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        progressBar.setVisibility(View.GONE);
                        person_contact=null;

                        if (task.isSuccessful()) {
                            Toast.makeText(getContext(), getString(R.string.registration_success), Toast.LENGTH_LONG).show();
                            setfragment(new Person_Place_Fragment());

                        } else {
                            //display a failure message
                            startActivity(new Intent(getActivity(), Home.class));
                            getActivity().finish();
                        }
                    }
                });

            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setfragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right, R.anim.slideout_from_left);
        fragmentTransaction.replace(R.id.activity_frame_layout, fragment);
        fragmentTransaction.commit();
    }
    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}