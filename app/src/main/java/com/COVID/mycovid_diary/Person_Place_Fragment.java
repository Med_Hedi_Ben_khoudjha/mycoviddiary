package com.COVID.mycovid_diary;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.COVID.MycovidDiary.Entity.SessionManagement;
import com.COVID.MycovidDiary.Entity.place;
import com.COVID.MycovidDiary.Home;
import com.COVID.MycovidDiary.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;


public class Person_Place_Fragment extends Fragment {
private EditText ville,commune,addres;
private Button valider;
    private ProgressBar progressBar;
    private FirebaseAuth auth;

    // TODO: Rename parameter arguments, choose names that match



    public Person_Place_Fragment() {
        // Required empty public constructor
    }






    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_person__place_, container, false);
        auth = FirebaseAuth.getInstance();
        progressBar=(ProgressBar) v.findViewById(R.id.progressBar);


        ville=(EditText)v.findViewById(R.id.ville);
        commune=(EditText)v.findViewById(R.id.commune);
        addres=(EditText)v.findViewById(R.id.address);
        valider=(Button)v.findViewById(R.id.valider);
    return v;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
valider.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        final String vill = ville.getText().toString().trim();
        final String commun = commune.getText().toString().trim();
        final String add = addres.getText().toString().trim();

        if (TextUtils.isEmpty(vill)) {
            Toast.makeText(getContext(), "Enter ville!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(commun)) {
            Toast.makeText(getContext(), "Enter commune!", Toast.LENGTH_SHORT).show();
            return;
        }
        if (TextUtils.isEmpty(add)) {
            Toast.makeText(getContext(), "Enter addresse complete!", Toast.LENGTH_SHORT).show();
            return;
        }
        progressBar.setVisibility(View.VISIBLE);
        SessionManagement sessionManagement = new SessionManagement(getActivity());
        sessionManagement.getSession();
        place place=new place(sessionManagement.getSession(),
                vill,
                commun,
                add);

        FirebaseDatabase.getInstance().getReference("place")
                .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                .setValue(place).addOnCompleteListener(new OnCompleteListener<Void>() {

            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                progressBar.setVisibility(View.GONE);

                if (task.isSuccessful()) {
                    Toast.makeText(getContext(), getString(R.string.registration_success), Toast.LENGTH_LONG).show();
                    startActivity(new Intent(getActivity(), StartingScreenActivity.class));
                    getActivity().finish();
                } else {
                    //display a failure message
                   // startActivity(new Intent(getActivity(), Home.class));
                    //getActivity().finish();
                }
            }
        });

    }
});
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setfragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right, R.anim.slideout_from_left);
        fragmentTransaction.replace(R.id.activity_frame_layout, fragment);
        fragmentTransaction.commit();
    }
    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
}