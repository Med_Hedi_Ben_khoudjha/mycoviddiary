package com.COVID.mycovid_diary;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.COVID.MycovidDiary.R;

public class StartingScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_screen);
       // Button buttonStartQuiz = findViewById(R.id.button_start_quiz);
        Button suivi = findViewById(R.id.suivi);
      //  buttonStartQuiz.setOnClickListener(new View.OnClickListener() {
        //    @Override
          //  public void onClick(View v) {
            //    startQuiz();
            //}
        //});
        suivi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartingScreenActivity.this, SuiviActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }
    private void startQuiz() {
        Intent intent = new Intent(StartingScreenActivity.this, QuizActivity.class);
        startActivity(intent);
    }
    }
