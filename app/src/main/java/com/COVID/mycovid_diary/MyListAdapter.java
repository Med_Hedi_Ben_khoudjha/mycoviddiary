package com.COVID.mycovid_diary;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.COVID.MycovidDiary.Entity.person_contact;
import com.COVID.MycovidDiary.R;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import java.util.ArrayList;

public class MyListAdapter extends RecyclerSwipeAdapter<MyListAdapter.ViewHolder> {
    private ArrayList<person_contact> listdata;
Context context;
    public MyListAdapter(Context context,ArrayList<person_contact> listdata) {
        this.listdata = listdata;
        this.context=context;
    }

    @NonNull
    @Override
    public MyListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.list_item, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference();
        Query applesQuery = (Query) ref.child("person_contact");

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyListAdapter.ViewHolder holder, final int position) {
        final person_contact myListData = listdata.get(position);
        holder.name.setText(listdata.get(position).getName_pre());
        holder.numero_per_cont.setText(listdata.get(position).getNumero());
        holder.circonstances_contact.setText(listdata.get(position).getCirc_cont());
        holder.currentDate.setText(listdata.get(position).getDate());

        holder.imageView.setImageResource(R.drawable.ic_place_black_24dp);
//        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(view.getContext(), "click on item: " + myListData.getEmail(), Toast.LENGTH_LONG).show();
//            }
//        });
        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        //dari kiri
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, holder.swipeLayout.findViewById(R.id.bottom_wrapper1));

        //dari kanan
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.swipeLayout.findViewById(R.id.bottom_wraper));

        holder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {

            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onClose(SwipeLayout layout) {

            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {

            }
        });
        holder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        holder.btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "Clicked on Information " + holder.name.getText().toString(), Toast.LENGTH_SHORT).show();
            }
        });

        holder.Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mItemManger.removeShownLayouts(holder.swipeLayout);
                listdata.remove(position);
                notifyItemRemoved(position);
                notifyItemRangeChanged(position, listdata.size());
                mItemManger.closeAllItems();
                
              //  jsonrequest(produit.getUrl());

            }
        });


        mItemManger.bindView(holder.itemView, position);


    }

    @Override
    public int getItemCount() {
        return listdata.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public SwipeLayout swipeLayout;


        public ImageView imageView;
        public TextView name,numero_per_cont,circonstances_contact,currentDate;
        public LinearLayout relativeLayout;
        public ImageButton btnLocation;

        public TextView Delete;


        public ViewHolder(View itemView) {
            super(itemView);
            this.imageView = (ImageView) itemView.findViewById(R.id.imageView);
            this.name = (TextView) itemView.findViewById(R.id.name);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);

            Delete = (TextView) itemView.findViewById(R.id.Delete);


            btnLocation = (ImageButton) itemView.findViewById(R.id.btnLocation);

            this.numero_per_cont = (TextView) itemView.findViewById(R.id.numero_per_cont);
            this.currentDate = (TextView) itemView.findViewById(R.id.currentDate);
            this.circonstances_contact = (TextView) itemView.findViewById(R.id.circonstances_contact);
          //  relativeLayout=(LinearLayout)itemView.findViewById(R.id.relativeLayout);
}
    }
}
