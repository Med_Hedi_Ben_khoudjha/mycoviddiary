package com.COVID.MycovidDiary.RegisterFragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.COVID.MycovidDiary.Entity.utilisateur;
import com.COVID.MycovidDiary.Home;
import com.COVID.MycovidDiary.R;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

/**
 * A simple {@link Fragment} subclass.
 */

public class RegisterFragment extends Fragment {
private EditText input_name,input_email,input_password;
private Button btn_signup;
private TextView link_login;


    private ProgressBar progressBar;
    private FirebaseAuth auth;
    public RegisterFragment() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View review=inflater.inflate(R.layout.fragment_register,container,false);
        auth = FirebaseAuth.getInstance();

        input_email=(EditText)review.findViewById(R.id.input_email);
        input_name=(EditText)review.findViewById(R.id.input_name);
        input_password=(EditText)review.findViewById(R.id.input_password);
        btn_signup=(Button)review.findViewById(R.id.btn_signup);
        link_login=(TextView)review.findViewById(R.id.link_login);
        progressBar=(ProgressBar) review.findViewById(R.id.progressBar);
        return review;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        link_login.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                setfragment(new LoginFragment());

            }
        });
        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String email = input_email.getText().toString().trim();
                final String password = input_password.getText().toString().trim();
                final String name = input_name.getText().toString().trim();


                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (password.length() < 6) {
                    Toast.makeText(getContext(), "Password too short, enter minimum 6 characters!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);
                //create user
                auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Toast.makeText(getContext(), "createUserWithEmail:onComplete:" + task.isSuccessful(), Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                if (!task.isSuccessful()) {
                                    Toast.makeText(getContext(), "Authentication failed." + task.getException(),
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    utilisateur user = new utilisateur(
                                            name,
                                            email,
                                            password
                                    );

                                    FirebaseDatabase.getInstance().getReference("Users")
                                            .child(FirebaseAuth.getInstance().getCurrentUser().getUid())
                                            .setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            progressBar.setVisibility(View.GONE);
                                            if (task.isSuccessful()) {
                                                Toast.makeText(getContext(), getString(R.string.registration_success), Toast.LENGTH_LONG).show();
                                            } else {
                                                //display a failure message
                                                startActivity(new Intent(getActivity(), Home.class));
                                                getActivity().finish();
                                            }
                                        }
                                    });

                                }
                            }
                        });

            }
        });


            }
    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setfragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right, R.anim.slideout_from_left);
        fragmentTransaction.replace(R.id.Register_frameLayout, fragment);
        fragmentTransaction.commit();
    }


}
