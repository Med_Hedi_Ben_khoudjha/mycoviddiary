package com.COVID.MycovidDiary.RegisterFragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.COVID.MycovidDiary.Entity.SessionManagement;
import com.COVID.MycovidDiary.MainActivity;
import com.COVID.MycovidDiary.R;
import com.COVID.MycovidDiary.User;


public class LogOutFragment extends Fragment {
Button logout;
    AlertDialog.Builder builder;


    public LogOutFragment() {
        // Required empty public constructor
    }





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_log_out, container, false);
        builder = new AlertDialog.Builder(getContext(),R.style.Theme_AppCompat_DayNight);

        logout=(Button)view.findViewById(R.id.logout);
    return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                builder.setMessage("Welcome to Alert Dialog") .setTitle("Javatpoint Alert Dialog");
                builder.setMessage("Do you want to close this application ?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                getActivity().finish();
                                Toast.makeText(getContext(),"you choose yes action for alertbox",
                                        Toast.LENGTH_SHORT).show();
                                //this method will remove session and open login screen
                                SessionManagement sessionManagement = new SessionManagement(getActivity());
                                sessionManagement.removeSession();

                                moveToLogin();
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                //  Action for 'NO' Button
                                dialog.cancel();
                                Toast.makeText(getContext(),"you choose no action for alertbox",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                //Creating dialog box
                AlertDialog alert = builder.create();
                //Setting the title manually
                alert.setTitle("Covid Dairy");
                alert.show();
            }
        });



            }


    private void moveToLogin() {
        Intent intent = new Intent(getContext(), User.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK| Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}