package com.COVID.MycovidDiary.RegisterFragment;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.widget.Toast;

import com.COVID.MycovidDiary.Entity.SessionManagement;
import com.COVID.MycovidDiary.Entity.utilisateur;
import com.COVID.MycovidDiary.Home;
import com.COVID.MycovidDiary.R;
import com.COVID.MycovidDiary.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Objects;


/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {
    private static final String TAG = "LoginActivity";
    private static final int REQUEST_SIGNUP = 0;


    private EditText _emailText,_passwordText;
    private Button _loginButton;
    private TextView _signupLink,__forgot;
    private FirebaseAuth auth;
    private ProgressBar progressBar;
    Button send;
    public LoginFragment() {
        // Required empty public constructor
    }


    @Override
    public void onStart() {
        super.onStart();

        checkSession();
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
View rootview=inflater.inflate(R.layout.fragment_login,container,false);
        _emailText=(EditText)rootview.findViewById(R.id.input_email);
        _passwordText=(EditText)rootview.findViewById(R.id.input_password);
        _loginButton=(Button)rootview.findViewById(R.id.btn_login);
        _signupLink=(TextView) rootview.findViewById(R.id.link_signup);
        progressBar=(ProgressBar) rootview.findViewById(R.id.progressBar);
        __forgot=(TextView)rootview.findViewById(R.id.link_forgot);
//Get Firebase auth instance
        auth = FirebaseAuth.getInstance();

        return  rootview;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        __forgot.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                setfragment(new ForgotFragment());

            }
        });
        _signupLink.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                setfragment(new RegisterFragment());
            }
        });
        _loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String email = _emailText.getText().toString();
                final String password = _passwordText.getText().toString();

                if (TextUtils.isEmpty(email)) {
                    Toast.makeText(getContext(), "Enter email address!", Toast.LENGTH_SHORT).show();
                    return;
                }

                if (TextUtils.isEmpty(password)) {
                    Toast.makeText(getContext(), "Enter password!", Toast.LENGTH_SHORT).show();
                    return;
                }

                progressBar.setVisibility(View.VISIBLE);
                // 1.log in to app and save session of user
                // 2. move to mainActivity

                //1. login and save session
                utilisateur user = new utilisateur(email,password);
                SessionManagement sessionManagement = new SessionManagement(getContext());
                sessionManagement.saveSession(user);

                //2. step
                moveToMainActivity();

                //authenticate user
                auth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                // If sign in fails, display a message to the user. If sign in succeeds
                                // the auth state listener will be notified and logic to handle the
                                // signed in user can be handled in the listener.
                                progressBar.setVisibility(View.GONE);
                                if (!task.isSuccessful()) {
                                    // there was an error
                                    if (password.length() < 6) {
                                        _passwordText.setError(getString(R.string.minimum_password));
                                    } else {
                                        Toast.makeText(getContext(), getString(R.string.auth_failed), Toast.LENGTH_LONG).show();
                                    }
                                } else {
                                    Intent intent = new Intent(getContext(), Home.class);
                                    startActivity(intent);
                                    getActivity().finish();
                                }
                            }
                        });

            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setfragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = requireActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right, R.anim.slideout_from_left);
        fragmentTransaction.replace(R.id.Register_frameLayout, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        progressBar.setVisibility(View.GONE);
    }
    private void checkSession() {
        //check if user is logged in
        //if user is logged in --> move to mainActivity

        SessionManagement sessionManagement = new SessionManagement(getActivity());
        String userID = sessionManagement.getSession();

        if(! userID.equals("-1")){
            //user id logged in and so move to mainActivity
            moveToMainActivity();
        }
        else{
            //do nothing
        }
    }


    private void moveToMainActivity() {
        Intent intent = new Intent(getContext(), Home.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

}
