package com.COVID.MycovidDiary;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentContainerView;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;

import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;
import android.widget.Toolbar;

import com.COVID.AccueilFragment.ContactFragment;
import com.COVID.AccueilFragment.HomeFragment;
import com.COVID.AccueilFragment.NotificationFragment;
import com.COVID.AccueilFragment.SettingFragment;
import com.COVID.AccueilFragment.SymptomsFragment;
import com.COVID.MycovidDiary.RegisterFragment.LogOutFragment;
import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseAuth;

import java.lang.reflect.Field;

public class Home extends AppCompatActivity {

    private FirebaseAuth auth;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private ActionBarDrawerToggle t;

    BottomNavigationView bottomNavigationView;
    FragmentContainerView fragmentContainerView;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_home);
        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
        {

            //Setting a dynamic title at runtime. Here, it displays the current time.
            actionBar.setTitle("Covid Diary");
        }

        setfragment(new HomeFragment());



        // bottomNavigationView = findViewById(R.id.activity_main_bottom_navigation);
        //      NavController navController = Navigation.findNavController(Home.this, R.id.fragment);
        //fragmentContainerView=findViewById(R.id.fragment);
//        NavigationUI.setupWithNavController(bottomNavigationView, navController);
        drawerLayout = (DrawerLayout) findViewById(R.id.activity_main_drawer_layout);
        t = new ActionBarDrawerToggle(this, drawerLayout, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawerLayout.addDrawerListener(t);
        t.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        //removeShiftMode(bottomNavigationView);
        navigationView = (NavigationView) findViewById(R.id.activity_main_nav_view);
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.homeFragment:
                        Toast.makeText(Home.this, "Home", Toast.LENGTH_SHORT).show();
                        setfragment(new HomeFragment());
                        break;
                    case R.id.symptomsFragment:
                        Toast.makeText(Home.this, "Symptoms", Toast.LENGTH_SHORT).show();
                        setfragment(new SymptomsFragment());
                        break;
                    case R.id.settingFragment:
                        Toast.makeText(Home.this, "Setting", Toast.LENGTH_SHORT).show();
                        setfragment(new SettingFragment());

                        break;
                    case R.id.contactFragment:
                        Toast.makeText(Home.this, "Contact", Toast.LENGTH_SHORT).show();
                        setfragment(new ContactFragment());

                        break;
                    case R.id.notificationFragment:
                        Toast.makeText(Home.this, "Notification", Toast.LENGTH_SHORT).show();
                        setfragment(new NotificationFragment());

                        break;
                    case R.id.logOutFragment:
                        Toast.makeText(Home.this, "Log out", Toast.LENGTH_SHORT).show();
                        setfragment(new LogOutFragment());

                        break;
                    default:
                        return true;
                }


                return true;

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (t.onOptionsItemSelected(item))
            return true;

        return super.onOptionsItemSelected(item);
    }

    //fragmentContainerView=findViewById(R.id.fragment);
    //  AppBarConfiguration appBarConfiguration =
    //            new AppBarConfiguration.Builder(R.id.homeFragment, R.id.symptomsFragment,R.id.settingFragment,R.id.contactFragment,R.id.notificationFragment).build();
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void setfragment(Fragment fragment) {

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.setCustomAnimations(R.anim.slide_right, R.anim.slideout_from_left);
        fragmentTransaction.replace(R.id.activity_main_frame_layout, fragment);
        fragmentTransaction.commit();
    }


    @Override
    public void onBackPressed() {
        // 5 - Handle back click to close menu
        if (this.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            this.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    // ---------------------
    // CONFIGURATION
    // ---------------------





   /* @SuppressLint("RestrictedApi")
    private void removeShiftMode(BottomNavigationView bottomNavigationView) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShifting(false);
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
        } catch (IllegalAccessException e) {
        }
    }
}*/
}