package com.COVID.MycovidDiary.Entity;

public class person_contact {

    String numero;
    String nombre;
    String name_pre;
    String circ_cont;
    String date;

    public person_contact() {
    }

    public String getEmail() {
        return email;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public person_contact(String numero, String nombre, String name_pre, String circ_cont, String email,String date) {
        this.numero = numero;
        this.nombre = nombre;
        this.name_pre = name_pre;
        this.circ_cont = circ_cont;
        this.email = email;
        this.date=date;
    }

    String email ;

    public person_contact(String numero, String nombre, String name_pre, String circ_cont) {
        this.numero = numero;
        this.nombre = nombre;
        this.name_pre = name_pre;
        this.circ_cont = circ_cont;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getName_pre() {
        return name_pre;
    }

    public void setName_pre(String name_pre) {
        this.name_pre = name_pre;
    }

    public String getCirc_cont() {
        return circ_cont;
    }

    public void setCirc_cont(String circ_cont) {
        this.circ_cont = circ_cont;
    }
}
