package com.COVID.MycovidDiary.Entity;

public class place {
    private String email;
    private String ville;
    private String commune;
    private String address;

    public place(String email, String ville, String commune, String address) {
        this.email = email;
        this.ville = ville;
        this.commune = commune;
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
